import numpy as np
import json
from matplotlib import pyplot as plt


class Measurement:
    def __init__(self,json):
        self.json = json
        self.json_opener()
        self.json_converter()
        self.json_plot()

    def json_opener(self):
        self.opener = open(self.json, )
        self.dog = json.load(self.opener)
        self.data = self.dog["data"]

    def json_converter(self):
        self.gravity = self.data["gravity"]
        self.gravity = [word for line in self.gravity for word in line.split()]
        self.gravity = [float(i) for i in self.gravity]
        self.s1 = len(self.gravity)
        self.gravity_array = np.array(self.gravity)
        self.gravity_array = np.hsplit(self.gravity_array, self.s1 / 4)
        self.gravity_array = np.stack(self.gravity_array, axis=1)
        #
        self.attitude = self.data["attitude"]
        self.attitude = [word for line in self.attitude for word in line.split()]
        self.attitude = [float(i) for i in self.attitude]
        self.s2 = len(self.attitude)
        self.attitude_array = np.array(self.attitude)
        self.attitude_array = np.hsplit(self.attitude_array, self.s2 / 5)
        self.attitude_array = np.stack(self.attitude_array, axis=1)
        #
        self.user_acceleration = self.data["user_acceleration"]
        self.user_acceleration = [word for line in self.user_acceleration for word in line.split()]
        self.user_acceleration = [float(i) for i in self.user_acceleration]
        self.s3 = len(self.user_acceleration)
        self.user_acceleration_array = np.array(self.user_acceleration)
        self.user_acceleration_array = np.hsplit(self.user_acceleration_array, self.s3 / 4)
        self.user_acceleration_array = np.stack(self.user_acceleration_array, axis=1)
        #
        self.rotation_rate = self.data["rotation_rate"]
        self.rotation_rate = [word for line in self.rotation_rate for word in line.split()]
        self.rotation_rate = [float(i) for i in self.rotation_rate]
        self.s4 = len(self.rotation_rate)
        self.rotation_rate_array = np.array(self.rotation_rate)
        self.rotation_rate_array = np.hsplit(self.rotation_rate_array, self.s4 / 4)
        self.rotation_rate_array = np.stack(self.rotation_rate_array, axis=1)

    def json_plot(self):
        fig1, axs1 = plt.subplots(2, 2)
        fig1.suptitle('Gravity')
        axs1[0, 0].plot(self.gravity_array[0,])
        axs1[0, 1].plot(self.gravity_array[1,])
        axs1[1, 0].plot(self.gravity_array[2,])
        axs1[1, 1].plot(self.gravity_array[3,])
        plt.savefig("figure1.png")

        fig2, axs2 = plt.subplots(2, 3)
        fig2.suptitle('Attitude')
        axs2[0, 0].plot(self.attitude_array[0,])
        axs2[0, 1].plot(self.attitude_array[1,])
        axs2[1, 0].plot(self.attitude_array[2,])
        axs2[1, 1].plot(self.attitude_array[3,])
        axs2[1, 2].plot(self.attitude_array[4,])
        plt.savefig("figure2.png")

        fig3, axs3 = plt.subplots(2, 2)
        fig3.suptitle('User acceleration')
        axs3[0, 0].plot(self.user_acceleration_array[0,])
        axs3[0, 1].plot(self.user_acceleration_array[1,])
        axs3[1, 0].plot(self.user_acceleration_array[2,])
        axs3[1, 1].plot(self.user_acceleration_array[3,])
        plt.savefig("figure3.png")

        fig4, axs4 = plt.subplots(2, 2)
        fig4.suptitle('Rotation rate')
        axs4[0, 0].plot(self.rotation_rate_array[0,])
        axs4[0, 1].plot(self.rotation_rate_array[1,])
        axs4[1, 0].plot(self.rotation_rate_array[2,])
        axs4[1, 1].plot(self.rotation_rate_array[3,])
        plt.savefig("figure4.png")

        plt.show()


Measurement('testosterone_ongoing/jason/SensDog_1_Tasli_2016-10-23_16_16_30.607.json')

path = 'testosterone_ongoing/jason/SensDog_1_Tasli_2016-10-23_16_16_30.607.json'

if _name_ == '_main_':
    Measurement(path)